# Tokenization demo

### Install dependencies
```shell script
$ npm i
```

### Compile contracts

```shell script
$ npm run sol:compile
```

### Start project

```shell script
$ npm start
```

Swagger API: http://localhost:3000/api-doc

### Deploy for development

```shell script
$ npm run sol:deploy [network]
```
