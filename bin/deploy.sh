rm -r ./build

truffle compile

truffle migrate --network "$1"

truffle run verify SteToken --network "$1"
