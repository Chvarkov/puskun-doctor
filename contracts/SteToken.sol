pragma solidity >=0.4.21 <0.7.0;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Mintable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Burnable.sol";

contract SteStableCurrencyInterface {
    function balanceOf(address whom) view public returns (uint);
}

contract ERC20Interface {
    function balanceOf(address whom) view public returns (uint);
}

contract ERC20BalanceChecker {
    function checkErc20Balance(address _tokenAddress, address _addressToQuery) view public returns (uint) {
        return ERC20Interface(_tokenAddress).balanceOf(_addressToQuery);
    }
}

/**
* @dev Investor role
*/
contract ERC20Investable is ERC20 {
    struct InvestorSettings {
        address account;
        uint invested;
        bool accredited;
    }

    uint unaccreditedLimit;

    using Roles for Roles.Role;

    event InvestorAdded(address indexed account);
    event InvestorRemoved(address indexed account);

    Roles.Role private _investors;

    constructor () internal {
        _addInvestor(_msgSender());
    }

    modifier onlyInvestor() {
        require(isInvestor(_msgSender()), "Investor: caller does not have the Minter role");
        _;
    }

    function isInvestor(address account) public view returns (bool) {
        return _investors.has(account);
    }

    function _addInvestor(address account) internal {
        _investors.add(account);
        emit InvestorAdded(account);
    }

    function _removeInvestor(address account) internal {
        _investors.remove(account);
        emit InvestorRemoved(account);
    }
}

contract BaseSteToken is ERC20, ERC20Detailed, ERC20Mintable, ERC20Burnable, ERC20BalanceChecker {
    address public owner;
    mapping (address => uint) public balances;

    string public bank;

    constructor(string memory _name, string memory _symbol, uint8 _decimals, string memory _bank)ERC20Detailed(_name, _symbol, _decimals) public {
        bank = _bank;
        owner = msg.sender;

    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
}

contract SteToken is BaseSteToken {
    enum Status {
        Published,
        Selling,
        Completed,
        Stopped
    }

    uint public minInvestAmount = 0.1 ether;

    Status public status;

    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        string memory _bank,
        uint256 _initialSupply
    )
    BaseSteToken(_name, _symbol, _decimals, _bank) public {
        owner = msg.sender;
        mint(address(this), _initialSupply);
    }

    function stopSelling() public onlyOwner {
        status = Status.Stopped;
    }

    function startSelling() public onlyOwner {
        status = Status.Selling;
    }

    function() external payable {
        require(msg.value >= minInvestAmount, 'Minimum limit.');

        transferFrom(address(this), msg.sender, msg.value);
    }

    function distribute() public onlyOwner {
//        transferFrom(address(this), issuer, address(this).balance);
        status = Status.Completed;
    }

    function refund() public onlyOwner {
        status = Status.Completed;
    }

    function sendToken(address receiver, uint amount) public returns(bool successful){
        if (balances[msg.sender] < amount) {
            return false;
        }
       return transferFrom(address(this), receiver, amount);
    }
}
