const SteToken = artifacts.require("./SteToken.sol");

module.exports = function(deployer) {
  const _name = 'DEV token';
  const _symbol = 'DEV';
  const _decimals = 8;
  const _bank = 'ATB Bank';
  const _supply = 100000000000;
  const _issuer = 0xA5E17b4D509c2f99442C5120ACAF7d998eF24b01;
  deployer.deploy(SteToken, _name, _symbol, _decimals, _bank, _supply);
};
