import { Module } from '@nestjs/common';
import { WalletGeneratorModule } from './modules/wallet-generator';
import { WalletController } from './controllers/wallet.controller';
import { TokenController } from './controllers/token.controller';
import { TokenFactoryModule } from './modules/token-factory';

const providerUrl = 'https://ropsten.infura.io/v3/fdf54f439d5b4256b5d43167b324135a';

@Module({
    imports: [
        WalletGeneratorModule.forRoot({providerUrl}),
        TokenFactoryModule.forRoot({
            walletMnemonic: 'help rice gas mean police first lift around oppose win digital census',
            ownerAddress: '0xe37d7129f9Ffd9b177C3b258A9C84119d7b431b0',
            networks: {
                ropsten: {
                    networkType: 'ethereum',
                    networkId: 3,
                    gas: 5500000,
                    confirmations: 2,
                    timeoutBlocks: 200,
                    providerUrl,
                },
            },
            network: 'ropsten',
            compiledContractDirectory: '/home/chvarkov/Projects/SecurityTokenPlatform/tokenization-demo/build/contracts',
            workDirectory: '/home/chvarkov/Projects/SecurityTokenPlatform/tokenization-demo',
        }),
    ],
    controllers: [
        WalletController,
        TokenController,
    ],
})
export class AppModule {}
