import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { TokenDeployer } from '../modules/token-factory';
import { DeployDto } from '../dto/deploy.dto';
import { TokenManager } from '../modules/token-factory/services/token.manager';
import { TokenAddressDto } from '../dto/token-address.dto';
import { BalanceImpactDto } from '../dto/balance-impact.dto';
import { BalanceOfDto } from '../dto/balance-of.dto';

@Controller('tokens')
export class TokenController {
    constructor(private readonly tokenDeployer: TokenDeployer,
                private readonly tokenManager: TokenManager) {
    }

    @Post('deploy')
    async deploy(@Body() deploy: DeployDto): Promise<any> {
        await this.tokenDeployer.deploy({
            name: deploy.name,
            symbol: deploy.symbol,
            bank: deploy.bank,
            decimals: deploy.decimals,
            initialSupply: 100000000000,
        })
    }

    @Get(':tokenAddress/:walletAddress/balance')
    async balance(@Param() params: BalanceOfDto): Promise<any> {
        return {
            balance: await this.tokenManager.balanceOf(params.tokenAddress, params.walletAddress),
        }
    }

    @Post(':address/mint')
    async mint(@Param() params: TokenAddressDto, @Body() impact: BalanceImpactDto): Promise<any> {
        await this.tokenManager.mint(params.address, impact.walletAddress, impact.amount);
    }

    @Post(':address/burn')
    async burn(@Param() params: TokenAddressDto, @Body() impact: BalanceImpactDto): Promise<any> {
        await this.tokenManager.burnFrom(params.address, impact.walletAddress, impact.amount);
    }

    @Post(':address/verify')
    async verify(@Param() params: TokenAddressDto): Promise<any> {
        await this.tokenManager.verify(params.address);
    }
}
