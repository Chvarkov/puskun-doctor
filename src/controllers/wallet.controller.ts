import { Controller, Post } from '@nestjs/common';
import { EthAccount, WalletGenerator } from '../modules/wallet-generator';

@Controller('wallets')
export class WalletController {
    constructor(private readonly walletGenerator: WalletGenerator) {
    }

    @Post()
    create(): EthAccount {
        return this.walletGenerator.generate();
    }
}
