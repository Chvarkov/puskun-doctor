import { ApiProperty } from '@nestjs/swagger';

export class BalanceImpactDto {
    @ApiProperty()
    walletAddress: string;

    @ApiProperty()
    amount: number;
}
