import { ApiProperty } from '@nestjs/swagger';

export class BalanceOfDto {
    @ApiProperty({description: 'Token address'})
    tokenAddress: string;

    @ApiProperty({description: 'Wallet address'})
    walletAddress: string;
}
