import { ApiProperty } from '@nestjs/swagger';

export class DeployDto {
    @ApiProperty({example: 'Example name'})
    name: string;

    @ApiProperty({example: 'EXMPL'})
    symbol: string;

    @ApiProperty({example: 8})
    decimals: number;

    @ApiProperty({example: 'Example bank'})
    bank: string;
}
