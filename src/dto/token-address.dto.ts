import { ApiProperty } from '@nestjs/swagger';

export class TokenAddressDto {
    @ApiProperty({description: 'Token address'})
    address: string;
}
