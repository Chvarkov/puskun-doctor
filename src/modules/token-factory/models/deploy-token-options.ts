export class DeployTokenOptions {
    name: string;
    symbol: string;
    decimals: number;
    bank: string;
    initialSupply: number;
}
