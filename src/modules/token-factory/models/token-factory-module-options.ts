export interface TokenFactoryModuleOptions extends ResolverOptions, DeployerOptions {
}

export interface ResolverOptions {
    workDirectory: string;
    compiledContractDirectory: string;
}

export interface DeployerOptions {
    networks: {[network: string]: EthNetwork};
    network: string;

    ownerAddress: string;
    walletMnemonic: string;
}

export interface EthNetwork {
    networkType: 'ethereum' | string;
    providerUrl: string;
    networkId: number;
    gas: number;
    confirmations: number;
    timeoutBlocks: number;
}
