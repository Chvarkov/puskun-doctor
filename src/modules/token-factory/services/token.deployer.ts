import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { DeployTokenOptions } from '../models/deploy-token-options';
import { DeployerOptions } from '../models/token-factory-module-options';
const Deployer = require("@truffle/deployer");
const HDWalletProvider = require('truffle-hdwallet-provider');
const Verifier = require('truffle-plugin-verify');

@Injectable()
export class TokenDeployer {
    constructor(private readonly options: DeployerOptions,
                private readonly resolver: {require: () => any}) {
    }

    async deploy(options: DeployTokenOptions): Promise<any> {
        const networkOptions = this.options.networks[this.options.network];

        if (!networkOptions) {
            throw new InternalServerErrorException(`Network options for '${this.options.network}' was not provided.`)
        }

        const hdWalletProvider = new HDWalletProvider(this.options.walletMnemonic, networkOptions.providerUrl);

        const Contracts = require("@truffle/workflow-compile");

        const configOptions = {
            contracts: [],
            api_keys: {
                etherscan: 'UCKGJ96VBAQAM2J1F836KJPG9W3JTRECIK'
            },
            basePath: __dirname + '/migrations',
            contracts_directory: "/home/chvarkov/Projects/SecurityTokenPlatform/tokenization-demo/contracts", // dir where contracts are located
            contracts_build_directory: '/home/chvarkov/Projects/SecurityTokenPlatform/tokenization-demo/build/contracts',
            logger: console,
            network: this.options.network,
            network_id: networkOptions.networkId,
            provider: networkOptions.providerUrl,
            networks: {
                [this.options.network]: {
                    provider: () => hdWalletProvider,
                    network_id: networkOptions.networkId,
                    gas: networkOptions.gas,
                    confirmations: networkOptions.confirmations,
                    timeoutBlocks: networkOptions.timeoutBlocks,
                    skipDryRun: true,
                },
            },
            reset: true,
            compilers: {
                solc: {
                    settings: {
                        optimizer: {
                            enabled: false,
                            runs: 200
                        },
                        evmVersion: "byzantium"
                    }
                }
            },
            // basePath: path.dirname(this.file)
        };


        await Contracts.compile(configOptions)
            .then(() => console.log("Compilation complete!"))
            .catch(e => console.error(e));


        //@ts-ignore
        const Migrations = this.resolver.require('./Migrations.sol');
        Migrations.configureNetwork({networkType: networkOptions.networkType, provider: hdWalletProvider, networks: configOptions.networks});
        Migrations.defaults({from: this.options.ownerAddress});

        //@ts-ignore
        const SteToken = this.resolver.require('./SteToken.sol');
        SteToken.configureNetwork({networkType: networkOptions.networkType, provider: hdWalletProvider, networks: configOptions.networks});
        SteToken.defaults({from: this.options.ownerAddress});

        configOptions.contracts = [
            Migrations,
            SteToken,
        ];

        const deployer = new Deployer(configOptions);

        deployer.deploy(Migrations);

        console.log(options);

        deployer.deploy(SteToken, options.name, options.symbol, options.decimals, options.bank, 100000000000);

        const deployed = await deployer.start();

        console.log(deployed);

        console.log('VERIFICATION ========================================');

        configOptions['_'] = ['Migrations', `SteToken@${deployed.address}`];

        console.log(await Verifier(configOptions));
    }
}
