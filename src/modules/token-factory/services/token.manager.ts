import { Injectable } from '@nestjs/common';
import Web3 from 'web3';
const Web3Constructor = require('web3');
const HDWalletProvider = require('truffle-hdwallet-provider');
const Verifier = require('truffle-plugin-verify');
import * as spawn from 'cross-spawn';
import { DeployerOptions } from '../models/token-factory-module-options';

@Injectable()
export class TokenManager {
    private readonly web3: Web3;
    private readonly wallet;
    constructor(private readonly options: DeployerOptions,
                private readonly providerUrl: string,
                private readonly mnemonic: string,
                private readonly compiledProvider: (name: string) => any) {
        this.wallet = new HDWalletProvider(mnemonic, providerUrl);
        this.web3 = new Web3Constructor(this.wallet);
        this.web3.defaultAccount = '0xe37d7129f9Ffd9b177C3b258A9C84119d7b431b0';
    }

    interact(address: string) {
        return new this.web3.eth.Contract(this.compiledProvider('SteToken').abi as any, address);
    }

    async balanceOf(tokenAddress: string, walletAddress: string): Promise<any> {
        const tokenContract = this.interact(tokenAddress);
        return tokenContract.methods.balanceOf(walletAddress).call();
    }

    async burnFrom(tokenAddress: string, walletAddress: string, amount: number): Promise<any> {
        const tokenContract = this.interact(tokenAddress);
        await tokenContract.methods.burnFrom(walletAddress, amount).send({
            from: '0xe37d7129f9Ffd9b177C3b258A9C84119d7b431b0',
            gas: 8000029,
            // data: this.compiledProvider('SteToken').bytecode,
        });
    }

    async mint(tokenAddress: string, walletAddress: string, amount: number): Promise<any> {
        const tokenContract = this.interact(tokenAddress);
        await tokenContract.methods.mint(walletAddress, amount).send(
            {
            from: '0xe37d7129f9Ffd9b177C3b258A9C84119d7b431b0',
            // gas: 4712388,
            // data: this.compiledContract.bytecode,
        }
        );
    }

    async verify(tokenAddress: string): Promise<any> {
        const networkOptions = this.options.networks[this.options.network];
        console.log(await Verifier({
            _: ['Migrations', 'SteToken'],
            api_keys: {
                etherscan: 'UCKGJ96VBAQAM2J1F836KJPG9W3JTRECIK'
            },
            // apiKey: 'UCKGJ96VBAQAM2J1F836KJPG9W3JTRECIK',
            logger: console,
            network: this.options.network,
            network_id: networkOptions.networkId,
            provider: networkOptions.providerUrl,
            basePath: '/home/chvarkov/Projects/SecurityTokenPlatform/tokenization-demo',
            contracts_build_directory: '/home/chvarkov/Projects/SecurityTokenPlatform/tokenization-demo/build/contracts',
            networks: {
                [this.options.network]: {
                    provider: () => this.wallet,
                    network_id: networkOptions.networkId,
                    gas: networkOptions.gas,
                    confirmations: networkOptions.confirmations,
                    timeoutBlocks: networkOptions.timeoutBlocks,
                    // skipDryRun: true,
                },
            },
            compilers: {
                solc: {
                    // version: "0.5.1",    // Fetch exact version from solc-bin (default: truffle's version)
                    // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
                    settings: {          // See the solidity docs for advice about optimization and evmVersion
                    optimizer: {
                        enabled: false,
                        runs: 200
                    },
                     evmVersion: "byzantium"
                    }
                }
            },
        }));
        // // const result = await spawn.sync('truffle', ['run', 'verify', 'SteToken', ], { stdio: 'inherit' });
        // //
        // // if (result.status !== 0) {
        // //     throw result.error || new Error(result);
        // // }
        // Verifier.verifyContract(this.interact(tokenAddress), {
        //     apiKey: 'UCKGJ96VBAQAM2J1F836KJPG9W3JTRECIK',
        // })
    }
}
