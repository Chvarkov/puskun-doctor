import { DynamicModule, Module, Provider } from '@nestjs/common';
import { TokenDeployer } from './services/token.deployer';
import { TokenFactoryModuleOptions } from './models/token-factory-module-options';
import { TokenManager } from './services/token.manager';
import { join } from 'path';
const Resolver = require("@truffle/resolver");

@Module({
})
export class TokenFactoryModule {
    static readonly contractName = 'SteToken';

    static forRoot(options: TokenFactoryModuleOptions): DynamicModule {
        const providers: Provider[] = [
            {
                provide: TokenDeployer,
                useFactory: () => {
                    const resolver = new Resolver({
                        working_directory: options.workDirectory,
                        contracts_build_directory: options.compiledContractDirectory
                    });

                    const {ownerAddress, network, networks, walletMnemonic} = options;

                    return new TokenDeployer({ownerAddress, network, networks, walletMnemonic}, resolver);
                }
            },
            {
                provide: TokenManager,
                useFactory: () => {
                    const compiledProvider = (name: string) => require(join(options.compiledContractDirectory, `${this.contractName}.json`));
                    const {ownerAddress, network, networks, walletMnemonic} = options;
                    return new TokenManager({ownerAddress, network, networks, walletMnemonic}, options.networks[options.network].providerUrl, options.walletMnemonic, compiledProvider);
                }
            }
        ];

        return {
            module: TokenFactoryModule,
            providers,
            exports: providers,
        };
    }
}
