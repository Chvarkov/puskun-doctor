export interface EthAccount {
    address: string;
    privateKey: string;
}
