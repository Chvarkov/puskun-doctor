import { Injectable } from '@nestjs/common';
import { EthAccount } from '../models/eth-account';
import Web3 from 'web3';
const Web3Constructor = require('web3');

@Injectable()
export class WalletGenerator {
    private readonly web3: Web3;
    constructor(private readonly providerUrl: string) {
        this.web3 = new Web3Constructor(providerUrl);
    }

    generate(entropy?: string): EthAccount {
        const account =  this.web3.eth.accounts.create(entropy);

        return {
            address: account.address,
            privateKey: account.privateKey,
        };
    }
}
