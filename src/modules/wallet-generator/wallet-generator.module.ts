import { DynamicModule, Module, Provider } from '@nestjs/common';
import { WalletGeneratorModuleOptions } from './models/wallet-generator-module-options';
import { WalletGenerator } from './services/wallet.generator';

@Module({
})
export class WalletGeneratorModule {
    static forRoot(options: WalletGeneratorModuleOptions): DynamicModule {
        const providers: Provider[] = [
            {
                provide: WalletGenerator,
                useFactory: () => new WalletGenerator(options.providerUrl),
            }
        ];

        return {
            module: WalletGeneratorModule,
            providers,
            exports: providers,
        };
    }
}
